from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.

# receipts list view aka home page
# def home(request):
#     home = Receipt.objects.all()
#     context = {
#         "home": home
#     }
#     return render(request, "receipt/list.html", context)


@login_required
def home(request):
    home = Receipt.objects.filter(purchaser=request.user)

    context = {
        "home": home
    }

    return render(request, "receipt/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form
    }

    return render(request, "receipt/create.html", context)


@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "category": category
    }

    return render(request, "category/list.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)

    context = {
        "account": account
    }

    return render(request, "account/list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form
    }

    return render(request, "category/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form
    }

    return render(request, "account/create.html", context)
